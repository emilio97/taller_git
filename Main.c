#include <Point.h>

int main(){
	printf("Programa para calcular la distancia euclidiana entre dos puntos (x,y,x)\n");
	
	Point p1;
	Point p2;
	Point p3;

	printf("Ingrese el primer punto en el formato x1,y1,x1:\n");
	scanf("%lf,%lf,%lf",&p1.x,&p1.y,&p1.z);	
	printf("Ingrese el segundo punto en el formato x2,y2,z2:\n");
	scanf("%lf,%lf,%lf",&p2.x,&p2.y,&p2.z);

	double r = calcularDistancia(p1, p2);
	printf("La distancia entre el punto (%.2f,%.2f,%.2f) y el punto (%.2f,%.2f,%.2f) es %.2f\n",p1.x,p1.y,p1.z,p2.x,p2.y,p2.z,r);

	p3 = punto_medio(p1, p2);
	
	printf("El punto medio entre el punto (%.2f,%.2f,%.2f) y el punto (%.2f,%.2f,%.2f) es (%.2f,%.2f,%.2f)\n",p1.x,p1.y,p1.z,p2.x,p2.y,p2.z,p3.x,p3.y,p3.z);
}
