/*
Definiciones de funciones
*/

#include <stdio.h>

typedef struct Point{
	double x;
	double y;
	double z;
}Point;

double calcularDistancia(Point p1, Point p2);

Point punto_medio(Point a, Point b);