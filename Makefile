CC=gcc
DIR=./includes/
CFLAGS=-c -lm -I$(DIR)


distanciaE: Main.o Point.o
	$(CC) -o distanciaE Main.o Point.o -lm -I$(DIR)

Main.o: Main.c $(DIR)/Point.h
	$(CC) $(CFLAGS) Main.c

Point.o: Point.c 
	$(CC) $(CFLAGS) Point.c

clean:
	rm *o distanciaE