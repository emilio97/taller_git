#include <Point.h>
#include <math.h>

double calcularDistancia(Point p1, Point p2){
	float resultado;
	float r1 = pow(p2.x-p1.x,2);
	float r2 = pow(p2.y-p1.y,2);
	float r3 = pow(p2.z-p1.z,2);
	resultado = r1+r2+r3;
	resultado = sqrt(resultado);	
	return resultado;
}
Point punto_medio(Point a, Point b){
	double nuevaX = (a.x+b.x)/2;
	double nuevaY = (a.y+b.y)/2;
	double nuevaZ = (a.z+b.z)/2;
	
	Point nuevoP;
	nuevoP.x = nuevaX;
	nuevoP.y = nuevaY;
	nuevoP.z = nuevaZ;
	
	return nuevoP;


}
